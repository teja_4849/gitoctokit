//
//  Node+CoreDataProperties.swift
//  GitOctoKit
//
//  Created by MALLOJJALA PAVAN TEJA on 4/2/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//
//

import Foundation
import CoreData


extension Node {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Node> {
        return NSFetchRequest<Node>(entityName: "Node")
    }

    @NSManaged public var descripiton: String?
    @NSManaged public var license: String?
    @NSManaged public var name: String?
    @NSManaged public var openIssueCount: String?
    @NSManaged public var permissions: String?

}
