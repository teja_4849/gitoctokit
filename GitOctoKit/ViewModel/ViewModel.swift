//
//  ViewModel.swift
//  GitOctoKit
//
//  Created by MALLOJJALA PAVAN TEJA on 4/1/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation
import UIKit
struct DetailsViewModel {
    private let details: Details
    
    
    init(details: Details) {
        self.details = details
    }
    
    public var name: String {
        return "Name: \(details.name ?? "")"
    }
    
    public var license: String {
        return "Licence:  \(details.license?.name ?? "")"
    }
    
    public var permissions: String {
        let admin = (details.permissions?.admin)! ? "Admin ✅" : "admin 🚫"
        let pull = (details.permissions?.pull)! ? "Pull ✅" : "Pull 🚫"
        let push = (details.permissions?.push)! ? "Push ✅" : "Push 🚫"
      
        return "Permissions: \(admin) \(push) \(pull)"
    }
    
    public var descripiton: String {
        return "desc: \(details.description ?? "")"
    }
    
    public var openIssueCount: String {
        return "open_Issue_Count:  \(details.open_issues_count ?? 0)"
    }
    
    
}
