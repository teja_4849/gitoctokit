//
//  Service.swift
//  GitOctoKit
//
//  Created by MALLOJJALA PAVAN TEJA on 4/1/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation
final class Service: NSObject {
    
    //    // Create a singleton instance
    static let sharedInstance: Service = {
        return Service()
    }()
    
    
    private let baseUrl = "https://api.github.com/orgs/octokit/repos"
    
    func fetchDetails( withQueryItmes queryItmes: [URLQueryItem], completionHandler: @escaping ([Details]?, Error?) -> Void) {
        guard let url = URL(string: baseUrl) else {return}
        print("fetching data for url: \(url)")
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {return}
        components.queryItems = queryItmes

        
        let task = URLSession.shared.dataTask(with: components.url!, completionHandler: { (data, response, error) in
            if let error = error {
                completionHandler(nil, error)
                print("Error with fetching users: \(error)")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    print("Error with the response, unexpected status code: \(String(describing: response))")
                    return
            }
            guard let data = data else {return}
           // print("yeay got response ", String(decoding: data, as: UTF8.self))
            do {
                let userdetails = try JSONDecoder().decode([Details].self, from: data)
                DispatchQueue.main.async {
                    
                    completionHandler(userdetails, nil)
                }
            } catch let jsonErr{
                print("Failed to decode:", jsonErr)
            }
        })
        task.resume()
}
    
    

}
