//
//  ViewController.swift
//  GitOctoKit
//
//  Created by MALLOJJALA PAVAN TEJA on 4/1/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit
import CoreData


class ViewController: UIViewController {

    
    private var detailsVM: [DetailsViewModel] = []
    private var detailsM: [Details] = []
    var queryItems = [URLQueryItem]()


    private let cellIdentifier = "nodecell"

    @IBOutlet weak var nodesList: UITableView!
    
    
    lazy var fetchedhResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Node.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "descripiton", ascending: true)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DBManager.sharedDBInstance.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
         frc.delegate = self
        return frc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        pageCountqueryItem(with: 1)
    }

    
    // MARK: service request
    func pageCountqueryItem (with count: Int) {

        do {
            try self.fetchedhResultController.performFetch()
            print("COUNT FETCHED FIRST: \(String(describing: self.fetchedhResultController.sections?[0].numberOfObjects))")
        } catch let error  {
            print("ERROR: \(error)")
        }
        
        let numberofPages = URLQueryItem(name: "page", value: "\(count)")
        let perPage = URLQueryItem(name: "per_page", value: "10")
        queryItems.removeAll()
        queryItems.append(numberofPages)
        queryItems.append(perPage)
        Service.sharedInstance.fetchDetails(withQueryItmes: queryItems) { [weak self] (details, err) in
            let dvm = details?.map({DetailsViewModel(details: $0)}) ?? []
            self?.detailsVM.append(contentsOf: dvm)
            
            self?.clearData()
            self?.saveInCoreDataWith(ViewModelArray:  self?.detailsVM ?? [])

            DispatchQueue.main.async {
                self?.nodesList.reloadData()
            }
        }
    }
    
    private func createNodeEntityFrom(viewModel: DetailsViewModel) -> NSManagedObject? {
        
        let context = DBManager.sharedDBInstance.persistentContainer.viewContext
        if let nodeEntity = NSEntityDescription.insertNewObject(forEntityName: "Node", into: context) as? Node {

            nodeEntity.descripiton = viewModel.descripiton
            nodeEntity.license = viewModel.license
            nodeEntity.permissions = viewModel.permissions
            nodeEntity.name = viewModel.name
            nodeEntity.openIssueCount = viewModel.openIssueCount

            return nodeEntity
        }
        return nil
    }
    
    private func saveInCoreDataWith(ViewModelArray: [DetailsViewModel]) {
        _ = ViewModelArray.map({createNodeEntityFrom(viewModel: $0)})
        do {
            try DBManager.sharedDBInstance.persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    private func clearData() {
        do {
            
            let context = DBManager.sharedDBInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Node.self))
            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
                DBManager.sharedDBInstance.saveContext()
            } catch let error {
                print("ERROR DELETING : \(error)")
            }
        }
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = fetchedhResultController.sections?.first?.numberOfObjects {
            return count
        }
//        return detailsVM.count
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? nodeCell else {
            fatalError("Issue dequeuing \(cellIdentifier)")
        }
        
        if let node = fetchedhResultController.object(at: indexPath) as? Node {
            cell.configureFromDB(node: node)
            
        }
        
//        cell.configure(detailsVM: detailsVM[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let detailCount = detailsVM.count
        if indexPath.row == detailCount - 1 {
            pageCountqueryItem(with: detailCount/10)

        }
    }

}

extension ViewController: NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            self.nodesList.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            self.nodesList.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.nodesList.endUpdates()
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        nodesList.beginUpdates()
    }
}
