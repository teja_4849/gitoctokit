//
//  Model.swift
//  GitOctoKit
//
//  Created by MALLOJJALA PAVAN TEJA on 4/1/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation

struct Details: Codable {
    let name: String?
    let open_issues_count: Int?
    let license: licence?
    let permissions: permissions?
    let description: String?
}

struct permissions: Codable {
    let admin: Bool?
    let push: Bool?
    let pull: Bool?
}

struct  licence: Codable {
    let key: String?
    let name: String?
    let spdx_id: String?
    let url: String?
    let node_id: String?
}
